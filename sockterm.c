#define _POSIX_C_SOURCE 200809L

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#define MAX_FDS 24
#define SH_BUF_MAX 2048
#define SOCKNAME "sock"
#define SHELL "/bin/sh"
// number of entries at the beginning of the poll array that are special
// (aren't socket connections)
// these are: the socket fd, and the SIGCHLD notification pipe
#define SFDN 2

struct pollfd fds[MAX_FDS];
int nfds;

int pspipe[2];

enum parse_stat {
	StatNone = 0,
	// connected process wanted to close or gave an invalid command
	StatClose,
	// unrecoverable error
	StatFatal,
};

enum parse_line {
	LineNone,
	// the connection should be closed
	LineClose,
	// shell script command
	LineShell,
};

struct parsefd {
	// last character was a '\n'
	bool was_newline;
	enum parse_line line_type;
	// pending shell script command
	char shbuf[MAX_FDS];
	int shbuf_i;
	// file opened from the connection's fd
	FILE *file;
};

int nparsefds = 0;
struct parsefd parsefds[MAX_FDS - SFDN];
// index in the pollfd array will be one higher than the corresponding index in
// the parsefd array because the former includes a fd for the socket itself
#define fd_to_parsefd(i) \
	(i - SFDN)

// currently tracked process
pid_t pid = -1;

int raised_sig = -1;
static void signal_handler(int signal)
{
	raised_sig = signal;
	if(raised_sig == SIGCHLD) {
		// write to pspipe so poll knows this happened
		if(write(pspipe[1], "\n", 1) == -1) {
			perror("write");
		}
	}
}

static bool exec_shbuf(char *shbuf)
{
	// temporarily turn off signal_handler for SIGCHLD
	struct sigaction oldaction;
	struct sigaction saction;
	saction.sa_handler = SIG_DFL;
	saction.sa_flags = 0;
	sigemptyset(&saction.sa_mask);
	sigaction(SIGCHLD, &saction, &oldaction);

	// kill the old process first

	if(pid != -1) {
		kill(pid, SIGTERM);

		int wstatus;
		do {
			if(waitpid(pid, &wstatus, 0) == -1) {
				if(errno == EINTR) {
					errno = 0;
					continue;
				} else {
					perror("waitpid");
					errno = 0;
					break;
				}
			}
		} while(!WIFEXITED(wstatus) && !WIFSIGNALED(wstatus));
	}

	// spawn the new process

	pid_t fpid = fork();
	if(fpid == -1) {
		perror("fork");
		goto err;
	}

	if(!fpid) {
		char *argv[4] = {
			SHELL,
			"-c",
			shbuf,
			NULL,
		};

		execvp(argv[0], argv);

		perror("execvp");
		exit(1);
	}

	pid = fpid;

	sigaction(SIGCHLD, &oldaction, NULL);
	return true;
err:
	sigaction(SIGCHLD, &oldaction, NULL);
	return false;
}

enum parse_stat parse_char(char c, int i)
{
	i = fd_to_parsefd(i);
	struct parsefd *pst = &parsefds[i];
	if(pst->was_newline) {
		// start of a new line, process the command character

		switch(c) {
			break; case 's': pst->line_type = LineShell;
			break; case 'x': pst->line_type = LineClose;
			break; default:
				if(fprintf(pst->file, "esyntax\n") < 0) {
					perror("fprintf");
					return StatFatal;
				}
				fflush(pst->file);

				return StatClose;
				break;
		}
	} else {
		if(c == '\n') {
			// end of a command

			if(pst->line_type == LineShell) {
				// add a NULL byte to the buffer and exec it
				pst->shbuf[pst->shbuf_i++] = '\0';
				exec_shbuf(pst->shbuf);
				pst->shbuf_i = 0;
				// notify all connections of the new process
				for(int j = 0; j < nfds - SFDN; j++) {
					if(fprintf(parsefds[j].file,
							"%c%d\n", (j==i)?'P':'p', pid) < 0) {
						perror("fprintf");
						return StatFatal;
					}
					fflush(parsefds[j].file);
				}
			} else if(pst->line_type == LineClose) {
				return StatClose;
			}
		} else {
			if(pst->line_type == LineShell) {
				// add to the buffer
				pst->shbuf[pst->shbuf_i++] = c;
			}
		}
	}

	pst->was_newline = (c == '\n');
	return StatNone;
}

// addfd and rmfd are only for connection fds

static bool addfd(struct pollfd add)
{
	FILE *file = fdopen(add.fd, "r+");
	if(file == NULL)
		return false;
	fds[nfds++] = add;
	parsefds[nparsefds++] = (struct parsefd){
		.was_newline = true,
		.line_type = LineNone,
		.shbuf_i = 0,
		.file = file,
	};

	return true;
}

static void rmfdnomsg(int i)
{
	close(fds[i].fd);
	// swap remove the poll entry
	fds[i] = fds[--nfds];
	// swap remove the parse entry
	int pi = fd_to_parsefd(i);
	parsefds[pi] = parsefds[--nparsefds];
}

static void rmfd(int i)
{
	// send a closing message to the connection
	FILE *file = parsefds[fd_to_parsefd(i)].file;
	fprintf(file, "x\n");
	fflush(file);

	rmfdnomsg(i);
}

// makes the fd nonblocking and cloexec
static inline bool config_fd(int fd)
{
	if(fcntl(fd, F_SETFD, FD_CLOEXEC) == -1) {
		perror("fcntl");
		return false;
	}
	int fl = fcntl(fd, F_GETFL);
	if(fl == -1) {
		perror("fcntl");
		return false;
	}
	if(fcntl(fd, F_SETFL, fl | O_NONBLOCK ) == -1) {
		perror("fcntl");
		return false;
	}
	return true;
}

static bool run(void)
{
	for(;;) {
		if(poll(fds, nfds, -1) == -1) {
			if(errno == EINTR && raised_sig == SIGCHLD) {
				errno = 0;
				// make sure nothing weird is happening with the revents
				// considering the error
				for(int i = 0; i < nfds; i++)
					fds[i].revents = 0;
				// this could cause bugs, beware
				goto child;
			}

			perror("poll");
			return false;
		}

		// an event for the socket fd was recieved
		if(fds[0].revents) {
			if(fds[0].revents & POLLERR) {
				fprintf(stderr, "poll fd error\n");
				return false;
			}

			int connfd;
			if((connfd = accept(fds[0].fd, NULL, NULL)) == -1) {
				perror("accept");
				continue;
			}

			if(!config_fd(connfd)) {
				close(connfd);
				continue;
			}

			if(nfds == MAX_FDS) {
				fprintf(stderr, "Maximum socket connections reached\n");
				write(connfd, "eagain\nx\n", sizeof("eagain\nx\n"));
				close(connfd);
				continue;
			}

			// append the socket connection
			if(addfd((struct pollfd){
				.fd = connfd,
				.events = POLLIN,
				.revents = 0,
			}) == false) {
				return false;
			}
		}

		// a SIGCHLD was recieved sometime before the last poll
		if(fds[1].revents) {
			if(fds[1].revents & POLLERR) {
				fprintf(stderr, "poll fd error\n");
				return false;
			}

child:;
			// read to the end of the pipe so the event doesn't keep getting
			// triggered
			char x;
			errno = 0;
			while(read(pspipe[0], &x, 1) == 1)
				;
			if(errno && errno != EAGAIN && errno != EWOULDBLOCK) {
				perror("read");
				return false;
			}
			errno = 0;

			int wstatus;
			if(waitpid(pid, &wstatus, WNOHANG) == -1) {
				perror("waitpid");
				pid = -1;
				continue;
			}

			if(WIFEXITED(wstatus) || WIFSIGNALED(wstatus)) {
				// send a message indicating death to all connections
				for(int i = 0; i < nfds - SFDN; i++) {
					if(fprintf(parsefds[i].file,
								"d\n") < 0) {
						perror("fprintf");
						return false;
					}
					fflush(parsefds[i].file);
				}
				pid = -1;
			}
		}

		// loop over the fds other than the socket one
		for(int i = nfds - 1; i >= SFDN; i--) {
			if(fds[i].revents) {
				if(fds[i].revents & POLLIN) {
					char c;
					int res;
					while((res = read(fds[i].fd, &c, 1)) != 0) {
						if(res == -1) {
							// stop error code from triggering later for these
							if(errno == EAGAIN || errno == EWOULDBLOCK) {
								res = 0;
								errno = 0;
							}

							break;
						}

						enum parse_stat pres = parse_char(c, i);
						if(pres == StatClose) {
							// if an invalid command was sent we now just have
							// to remove the fd

							rmfd(i);
							goto iter_end;
						} else if(pres == StatFatal) {
							return false;
						}
					}

					if(res == -1) {
						perror("read");
						rmfd(i);
						continue;
					}
				}

				if(fds[i].revents & (POLLHUP | POLLERR)) {
					rmfdnomsg(i);
					continue;
				}

			}
		}
iter_end:;
	}

	return true;
}

int main(int argc, char **argv)
{
	(void)argc;
	(void)argv;
	struct sigaction saction;
	saction.sa_handler = signal_handler;
	saction.sa_flags = 0;
	sigemptyset(&saction.sa_mask);
	sigaction(SIGCHLD, &saction, NULL);

	int sockfd = socket(AF_UNIX, SOCK_STREAM, 0);
	if(sockfd == -1) {
		perror("socket");
		return 1;
	}

	if(fcntl(sockfd, F_SETFD, FD_CLOEXEC) == -1) {
		perror("fcntl");
		return false;
	}

	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(addr));
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, SOCKNAME);

	if(bind(sockfd, (const struct sockaddr *)&addr, sizeof(addr)) == -1) {
		perror("bind");
		close(sockfd);
		return 1;
	}

	if(listen(sockfd, 20) == -1) {
		perror("listen");
		close(sockfd);
		return 1;
	}

	// add the socket fd
	fds[nfds++] = (struct pollfd){
		.fd = sockfd,
		.events = POLLIN,
		.revents = 0,
	};

	// create the pipe for notifying of a SIGCHLD signal
	if(pipe(pspipe) == -1) {
		perror("pipe");
		close(sockfd);
		return 1;
	}

	// make both ends of the pipe nonblocking and cloexec
	if(!config_fd(pspipe[0]) || !config_fd(pspipe[1])) {
		close(pspipe[0]);
		close(pspipe[1]);
		close(sockfd);
		return 1;
	}

	// add the pipe fd
	fds[nfds++] = (struct pollfd){
		.fd = pspipe[0],
		.events = POLLIN,
		.revents = 0,
	};

	bool runerr = run();

	close(sockfd);
	close(pspipe[0]);
	close(pspipe[1]);

	if(!runerr) {
		return 0;
	} else {
		for(int i = 0; i < nfds; i++)
			close(fds[i].fd);

		return 1;
	}
}
